import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../Screen/Home';
import GameDetails from '../Screen/GameDetails';
import ByGenre from '../Screen/ByGenre';

const Stack = createStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator initialRouteName="Home" headerShown="false">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="GameDetails"
        component={GameDetails}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ByGenre"
        component={ByGenre}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default HomeStack;
