const initialState = {
  allGamesData: [],
  gameDetails: [],
  gameGenreData: [],
  gameId: '',
  isLoading: false,
  loadingMore: false,
  pageCount: 0,
};

const game = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_ALLGAME':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_ALLGAME_SUCCESS':
      return {
        ...state,
        allGamesData: action.data,
        isLoading: false,
      };
    case 'GET_GENRE':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_GENRE_SUCCESS':
      return {
        ...state,
        gameGenreData: action.data,
        isLoading: false,
      };
    case 'GET_GAME_ID':
      return {
        ...state,
      };
    case 'GET_GAME_ID_SUCCESS':
      return {
        ...state,
        gameId: action.gameId,
        isLoading: false,
      };
    case 'GET_GAME_DETAILS':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_GAME_DETAILS_SUCCESS':
      return {
        ...state,
        gameDetails: action.data,
        isLoading: false,
      };
    case 'GET_MORE_GAME':
      return {
        ...state,
        loadingMore: true,
        pageCount: state.pageCount + 1,
      };
    case 'GET_MORE_GAME_SUCCESS':
      return {
        ...state,
        allGamesData: [...state.allGamesData, ...action.data],
        loadingMore: false,
      };
    case 'GET_MORE_GAME_FAILED':
      return {
        ...state,
        pageCount: state.pageCount - 1,
        loadingMore: false,
      };

    default:
      return state;
  }
};

export default game;
