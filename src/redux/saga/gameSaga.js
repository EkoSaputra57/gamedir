import axios from 'axios';
import {takeLatest, put, select} from '@redux-saga/core/effects';

function* getAllGame(action) {
  try {
    console.log('action all game', action);
    const resAllGamesData = yield axios.get(
      'https://api.rawg.io/api/games?key=26ebb8d3ca814c95b7778ddeece62b7f',
    );
    yield put({
      type: 'GET_ALLGAME_SUCCESS',
      data: resAllGamesData.data.results,
    });
  } catch (err) {
    console.log('>>', err);
  }
}

function* getGameGenre(action) {
  console.log('mulai get genre');
  try {
    console.log('action get genre', action);
    const resGameGenreData = yield axios.get(
      'https://api.rawg.io/api/genres?key=26ebb8d3ca814c95b7778ddeece62b7f',
    );
    yield put({
      type: 'GET_GENRE_SUCCESS',
      data: resGameGenreData.data.results,
    });
  } catch (err) {
    console.log('>>', err);
  }
}

function* getGameDetails(action) {
  console.log('action game details', action);
  try {
    const resGameDetailsData = yield axios.get(
      `https://api.rawg.io/api/games/${action.id}?key=26ebb8d3ca814c95b7778ddeece62b7f`,
    );
    yield put({
      type: 'GET_GAME_DETAILS_SUCCESS',
      data: resGameDetailsData.data,
    });
    console.log('get det sukses');
  } catch (err) {
    console.log('>>', err);
  }
}

function* getMoreGame(action) {
  const page = yield select(state => state.game.pageCount);
  try {
    console.log('mulai get more game');
    const resGetMoreGame = yield axios.get(
      `https://api.rawg.io/api/games?key=26ebb8d3ca814c95b7778ddeece62b7f&page=${
        1 + page
      }`,
    );
    yield put({
      type: 'GET_MORE_GAME_SUCCESS',
      data: resGetMoreGame.data.results,
    });
    console.log('sukses bro', resGetMoreGame.data.results);
  } catch (err) {
    console.log('gagal bro', err);
    yield put({type: 'GET_MORE_GAME_FAILED'});
  }
}

export default function* gameSaga() {
  yield takeLatest('GET_ALLGAME', getAllGame);
  yield takeLatest('GET_GAME_DETAILS', getGameDetails);
  yield takeLatest('GET_GENRE', getGameGenre);
  yield takeLatest('GET_MORE_GAME', getMoreGame);
}
