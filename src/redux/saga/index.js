import {all} from 'redux-saga/effects';
import gameSaga from './gameSaga';

export default function* rootSagas() {
  yield all([gameSaga()]);
}
