import React from 'react';
import Header from '../Component/Header';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  FlatList,
  ImageBackground,
} from 'react-native';
import {useSelector} from 'react-redux';
import Load from '../Component/Load';
import moment from 'moment';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const GameDetails = () => {
  const gameDetails_redux = useSelector(state => state.game.gameDetails);
  const loading = useSelector(state => state.game.isLoading);

  const platfrom = gameDetails_redux.platforms;

  const renderPlatform = ({item, index}) => {
    return <Text style={{color: 'white'}}>{item.platform.name}</Text>;
  };

  const genre = gameDetails_redux.genres;

  const rendergenre = ({item, index}) => {
    return <Text style={{color: 'white'}}>{item.name}</Text>;
  };

  return (
    <ImageBackground
      source={{
        uri: gameDetails_redux.background_image,
      }}
      resizeMode="cover"
      style={style.BG}>
      {loading == false ? (
        <View style={{backgroundColor: 'rgba(52, 52, 52, 0.8)'}}>
          <ScrollView>
            <Header title="Game Details" />
            <View style={{alignItems: 'center'}}>
              <Text style={style.Title}>{gameDetails_redux.name}</Text>
              <Image
                source={{
                  uri: gameDetails_redux.background_image_additional,
                }}
                style={style.DisplayVid}
              />
            </View>
            <View style={style.About}>
              <Text style={style.BoldText}>About</Text>
              <Text style={{textAlign: 'justify', color: 'white'}}>
                {gameDetails_redux.description_raw}
              </Text>
            </View>
            <View style={style.ToRow}>
              <View style={style.Restrict}>
                <Text style={style.BoldText}>Platforms</Text>
                <FlatList
                  data={platfrom}
                  keyExtractor={(elem, i) => i}
                  renderItem={renderPlatform}
                />
              </View>
              <View style={style.Restrict}>
                <Text style={style.BoldText}>Genre</Text>
                <FlatList
                  data={genre}
                  keyExtractor={(elem, i) => i}
                  renderItem={rendergenre}
                />
              </View>
            </View>
            <View style={style.ToRow}>
              <View style={style.Restrict}>
                <Text style={style.BoldText}>Release Date</Text>
                <Text style={{color: 'white'}}>
                  {moment(gameDetails_redux.released).format('MMM Do YYYY')}
                </Text>
              </View>
              <View style={style.Restrict}>
                <Text style={style.BoldText}>Developer</Text>
                <Text style={{color: 'white'}}>
                  {gameDetails_redux.developers[0].name}
                </Text>
              </View>
            </View>
            <View style={style.ToRow}>
              <View style={style.Restrict}>
                <Text style={style.BoldText}>Publisher</Text>
                <Text style={{color: 'white'}}>
                  {gameDetails_redux.publishers[0].name}
                </Text>
              </View>
              <View style={style.Restrict}>
                <Text style={style.BoldText}>Age Rating</Text>
                <Text style={{color: 'white'}}>
                  {gameDetails_redux.esrb_rating.name}
                </Text>
              </View>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={style.BoldText}>Rating</Text>
              <View style={style.Star}>
                <MaterialCommunityIcons name="star" size={25} color="gold" />
                <Text
                  style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
                  {gameDetails_redux.rating}/5
                </Text>
              </View>
            </View>
          </ScrollView>
        </View>
      ) : (
        <Load />
      )}
    </ImageBackground>
  );
};

export default GameDetails;

const style = StyleSheet.create({
  ToRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    margin: 30,
    color: 'white',
  },
  DisplayVid: {
    height: 250,
    width: '90%',
    margin: 20,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: 'white',
  },
  About: {
    marginLeft: 30,
    marginRight: 30,
    color: 'white',
  },
  Restrict: {
    width: 180,
  },
  BoldText: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
  Title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 20,
    color: 'white',
  },
  Star: {
    flexDirection: 'row',
    marginBottom: 20,
    marginTop: 10,
    color: 'white',
  },
  BG: {
    flex: 1,
    justifyContent: 'center',
  },
});
