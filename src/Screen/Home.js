import React from 'react';
import {View, ImageBackground, StyleSheet} from 'react-native';
import Hot from '../Component//Hot';
import Header from '../Component/Header';
import GenreChoose from '../Component/GenreChoose';

const Home = () => {
  return (
    <ImageBackground
      source={{
        uri: 'https://media.rawg.io/media/games/157/15742f2f67eacff546738e1ab5c19d20.jpg',
      }}
      resizeMode="cover"
      style={style.BG}>
      <View style={{backgroundColor: 'rgba(52, 52, 52, 0.8)'}}>
        <Header title="Hot Games" />
        <View style={{flexDirection: 'row'}}>
          <Hot />
          <GenreChoose />
        </View>
      </View>
    </ImageBackground>
  );
};

export default Home;

const style = StyleSheet.create({
  BG: {
    flex: 1,
    justifyContent: 'center',
  },
});
