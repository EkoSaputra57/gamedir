import React, {useEffect} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import Header from '../Component/Header';
import Load from '../Component/Load';
import {useDispatch, useSelector} from 'react-redux';
import GenreChoose from '../Component/GenreChoose';
import {useRoute} from '@react-navigation/native';
import GenreList from '../Component/GenreList';

const ByGenre = () => {
  const dispatch = useDispatch();
  const route = useRoute();
  const byGenre_redux = useSelector(state => state.game.gameGenreData);
  const genrenya = route.params.genrenya;

  const genre = byGenre_redux.filter(item => item.name == genrenya);

  const renderAllGame = ({item, index}) => {
    return <GenreList name={item.name} id={item.id} />;
  };

  useEffect(() => {
    dispatch({type: 'GET_GENRE'});
  }, []);

  useEffect(() => {}, [byGenre_redux]);

  return (
    <View>
      {byGenre_redux.length == 0 ? (
        <Load />
      ) : (
        <View style={{backgroundColor: 'rgba(52, 52, 52, 0.8)'}}>
          <Header title={genrenya} />
          <View style={{flexDirection: 'row'}}>
            <GenreChoose />
            <FlatList
              data={genre[0].games}
              keyExtractor={(elem, i) => i}
              renderItem={renderAllGame}
            />
          </View>
        </View>
      )}
    </View>
  );
};

export default ByGenre;

const style = StyleSheet.create({
  BG: {
    flex: 1,
    justifyContent: 'center',
  },
});
