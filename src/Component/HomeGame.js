import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const HomeGame = props => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const handleDetails = async () => {
    await dispatch({type: 'GET_GAME_DETAILS', id: props.id});
    navigation.navigate('GameDetails');
  };

  return (
    <View>
      <TouchableOpacity
        style={style.reviewCard}
        onPress={() => handleDetails()}>
        <Image
          style={style.HWimage}
          source={{
            uri: props.image,
          }}
        />
        <Text style={style.title}>{props.title}</Text>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text style={style.BoldText}>Rating</Text>
          <View style={style.Star}>
            <MaterialCommunityIcons name="star" size={25} color="gold" />
            <Text style={{fontSize: 16, fontWeight: 'bold'}}>
              {props.rating}/5
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default HomeGame;

const style = StyleSheet.create({
  reviewCard: {
    backgroundColor: 'white',
    marginTop: 15,
    marginBottom: 15,
    marginRight: 5,
    marginLeft: 10,
    elevation: 10,
    borderRadius: 8,
    width: 220,
    height: 300,
    alignItems: 'center',
  },
  HWimage: {
    height: 180,
    width: 220,
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8,
  },
  title: {
    margin: 5,
    fontSize: 20,
    fontWeight: 'bold',
  },
  BoldText: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  Star: {
    flexDirection: 'row',
    marginBottom: 20,
    alignItems: 'center',
  },
});
