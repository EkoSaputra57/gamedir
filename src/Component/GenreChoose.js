import React, {useEffect} from 'react';
import {View, Text, StyleSheet, FlatList, ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Btn from './Btn';
import {useNavigation} from '@react-navigation/native';

const GenreChoose = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const gameGenre_redux = useSelector(state => state.game.gameGenreData);

  const renderGenre = ({item, index}) => {
    return (
      <Btn
        title={item.name}
        onPress={() => navigation.navigate('ByGenre', {genrenya: item.name})}
      />
    );
  };

  useEffect(() => {
    dispatch({type: 'GET_GENRE'});
  }, []);

  useEffect(() => {}, [gameGenre_redux]);

  return (
    <ScrollView>
      <View style={style.choose}>
        <Text style={style.genre}> Choose By</Text>
        <Text style={style.genre}>Genre</Text>
        <FlatList
          data={gameGenre_redux}
          keyExtractor={(elem, i) => i}
          renderItem={renderGenre}
        />
      </View>
    </ScrollView>
  );
};

export default GenreChoose;

const style = StyleSheet.create({
  genre: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
  },
  choose: {
    marginRight: 18,
    alignItems: 'center',
    marginTop: 20,
    height: 1050,
  },
});
