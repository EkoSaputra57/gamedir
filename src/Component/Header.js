import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';

const Header = props => {
  const navigation = useNavigation();
  return (
    <View style={style.BGcolor}>
      <AntDesign
        name="left"
        size={23}
        color="white"
        onPress={() => navigation.goBack()}
        style={{position: 'absolute', marginLeft: 10}}
      />
      <View
        style={{
          alignItems: 'center',
          width: '100%',
        }}>
        <Text style={style.text}>{props.title}</Text>
      </View>
    </View>
  );
};

export default Header;

const style = StyleSheet.create({
  BGcolor: {
    backgroundColor: 'rgb(176,176,176)',
    flexDirection: 'row',
    alignItems: 'center',
    height: 45,
    width: '100%',
    marginTop: 10,
  },
  text: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    justifyContent: 'center',
  },
});
