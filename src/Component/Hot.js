import React, {useEffect} from 'react';
import {View, FlatList} from 'react-native';
import Load from './Load';
import Btn from './Btn';
import HomeGame from './HomeGame';
import {useDispatch, useSelector} from 'react-redux';

const Hot = () => {
  const dispatch = useDispatch();
  const allGame_redux = useSelector(state => state.game.allGamesData);

  const renderAllGame = ({item, index}) => {
    return (
      <HomeGame
        title={item.name}
        image={item.background_image}
        id={item.id}
        rating={item.rating}
      />
    );
  };

  const getMoreGame = () => {
    dispatch({type: 'GET_MORE_GAME'});
  };

  useEffect(() => {
    dispatch({type: 'GET_ALLGAME'});
  }, []);

  useEffect(() => {}, [allGame_redux]);

  return (
    <View style={{marginBottom: 100}}>
      {allGame_redux.length == 0 ? (
        <Load />
      ) : (
        <FlatList
          data={allGame_redux}
          keyExtractor={(elem, i) => i}
          renderItem={renderAllGame}
          ListFooterComponent={
            <>
              <View>
                <Btn title="Load More" onPress={() => getMoreGame()} />
              </View>
            </>
          }
        />
      )}
    </View>
  );
};

export default Hot;
