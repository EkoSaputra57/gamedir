import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';

const GenreList = props => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const handleDetails = async () => {
    await dispatch({type: 'GET_GAME_DETAILS', id: props.id});
    navigation.navigate('GameDetails');
  };

  return (
    <View
      style={{
        width: 220,
        marginTop: 30,
      }}>
      <TouchableOpacity
        style={{flexDirection: 'row', alignItems: 'center'}}
        onPress={() => handleDetails()}>
        <MaterialCommunityIcons
          name="microsoft-xbox-controller"
          size={18}
          color="white"
          style={{marginRight: 5}}
        />
        <Text style={{fontSize: 18, color: 'white'}}>{props.name}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default GenreList;
